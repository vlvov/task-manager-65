package ru.t1.vlvov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDTO> {
    @Nullable ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @NotNull String description
    );

}
