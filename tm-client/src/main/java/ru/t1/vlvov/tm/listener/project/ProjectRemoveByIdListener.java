package ru.t1.vlvov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Remove project by Id.";

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setProjectId(id);
        projectEndpoint.removeById(request);
    }

}