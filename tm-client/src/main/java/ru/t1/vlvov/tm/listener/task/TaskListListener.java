package ru.t1.vlvov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.dto.request.TaskListRequest;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Display all tasks.";

    @NotNull
    private final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(CustomSort.values()));
        @Nullable final String sortName = TerminalUtil.nextLine();
        @Nullable final CustomSort sort = CustomSort.toSort(sortName);
        @Nullable final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @NotNull final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        renderTasks(tasks);
    }

}