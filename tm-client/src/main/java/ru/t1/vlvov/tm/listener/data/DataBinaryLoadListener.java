package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBinaryLoadRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    private final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBinaryLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        domainEndpoint.loadDataBinary(request);
    }

}
