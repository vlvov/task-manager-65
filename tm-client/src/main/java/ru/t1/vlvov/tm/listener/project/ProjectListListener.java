package ru.t1.vlvov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.request.ProjectListRequest;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Display all projects.";

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectListListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(CustomSort.values()));
        @Nullable final String sortName = TerminalUtil.nextLine();
        @Nullable final CustomSort sort = CustomSort.toSort(sortName);
        @Nullable final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @NotNull final List<ProjectDTO> projects = projectEndpoint.listProject(request).getProjects();
        for (@Nullable final ProjectDTO project : projects) {
            showProject(project);
        }
    }

}