package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.client.TaskEndpointClient;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.model.Task;

import java.util.Collection;

public class TaskEndpointTest {

    final static String BASE_URL = "http://localhost:8080/api/tasks";

    @NotNull
    final TaskEndpointClient client = TaskEndpointClient.client(BASE_URL);

    @NotNull
    final Task task1 = new Task("Task1");

    @NotNull
    final Task task2 = new Task("Task2");

    @NotNull
    final Task task3 = new Task("Task3");

    @Before
    public void init() {
        client.save(task1);
        client.save(task2);
        client.save(task3);
    }

    @After
    public void destroy() {
        if (client.findById(task1.getId()) != null)
            client.delete(task1);
        if (client.findById(task2.getId()) != null)
            client.delete(task2);
        if (client.findById(task3.getId()) != null)
            client.delete(task3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        Task task = new Task("New Task");
        client.save(task);
        Assert.assertEquals(task.getId(), client.findById(task.getId()).getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void delete() {
        Assert.assertNotNull(client.findById(task1.getId()));
        client.delete(task1);
        Assert.assertNull(client.findById(task1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        Assert.assertNotNull(client.findById(task1.getId()));
        client.delete(task1.getId());
        Assert.assertNull(client.findById(task1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        Collection<Task> tasks = client.findAll();
        Assert.assertTrue(tasks.size() >= 3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        Assert.assertNotNull(client.findById(task1.getId()));
    }

}
