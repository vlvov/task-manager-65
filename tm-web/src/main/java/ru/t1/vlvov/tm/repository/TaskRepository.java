package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    Task findFirstById(@NotNull final String id);

}
