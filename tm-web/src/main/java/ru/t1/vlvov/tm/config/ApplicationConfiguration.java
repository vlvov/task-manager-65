package ru.t1.vlvov.tm.config;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@ComponentScan("ru.t1.vlvov.tm")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.t1.vlvov.tm.repository")
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @Value("#{environment['database.driver']}") String databaseDriver,
            @Value("#{environment['database.url']}") String databaseUrl,
            @Value("#{environment['database.user']}") String databaseUser,
            @Value("#{environment['database.password']}") String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClass(databaseDriver);
        dataSource.setJdbcUrl(databaseUrl);
        dataSource.setUser(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("#{environment['database.dialect']}") String databaseDialect,
            @Value("#{environment['database.hbm2ddl_auto']}") String databaseHbm2ddl,
            @Value("#{environment['database.show_sql']}") String databaseShowSql,
            @Value("#{environment['database.second_lvl_cash']}") String databaseSecondLvlCash,
            @Value("#{environment['database.factory_class']}") String databaseFactoryClass,
            @Value("#{environment['database.use_query_cash']}") String databaseUseQueryCash,
            @Value("#{environment['database.use_min_puts']}") String databaseUseMinPuts,
            @Value("#{environment['database.region_prefix']}") String databaseRegionPrefix,
            @Value("#{environment['database.config_file_path']}") String databaseConfigFilePath,
            @Value("#{environment['database.format_sql']}") String databaseFormatSql
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.vlvov.tm.model");
        @NotNull final Properties settings = new Properties();
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseDialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseHbm2ddl);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseShowSql);
        //settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseSecondLvlCash);
        //settings.put(Environment.CACHE_REGION_FACTORY, databaseFactoryClass);
        //settings.put(Environment.USE_QUERY_CACHE, databaseUseQueryCash);
        //settings.put(Environment.USE_MINIMAL_PUTS, databaseUseMinPuts);
        //settings.put(Environment.CACHE_REGION_PREFIX, databaseRegionPrefix);
        //settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseConfigFilePath);
        settings.put(Environment.FORMAT_SQL, databaseFormatSql);
        factoryBean.setJpaProperties(settings);
        return factoryBean;
    }

}
