package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.vlvov.tm.api.IProjectEndpoint;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    ProjectService projectService;

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(
            @PathVariable("id") String id
    ) {
        return projectService.findOneById(id);
    }

}
