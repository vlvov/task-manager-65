package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        projectRepository.deleteById(id);
    }

    public @Nullable Project findOneById(@Nullable String id) {
        return projectRepository.findFirstById(id);
    }

}
