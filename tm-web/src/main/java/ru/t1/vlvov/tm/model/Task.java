package ru.t1.vlvov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.vlvov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
public class Task {

    @NotNull
    @Id
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
