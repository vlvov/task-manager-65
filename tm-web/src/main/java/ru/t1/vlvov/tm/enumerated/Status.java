package ru.t1.vlvov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import lombok.Getter;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private String displayName;

    Status(final @NotNull String displayName) {
        this.displayName = displayName;
    }

}
