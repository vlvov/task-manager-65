package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    Project findFirstById(@NotNull final String id);

}
