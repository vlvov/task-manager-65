package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.TaskRepository;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Nullable
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        taskRepository.deleteById(id);
    }

    public @Nullable Task findOneById(@Nullable String id) {
        return taskRepository.findFirstById(id);
    }


}
