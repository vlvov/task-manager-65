package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Task;

import java.util.Collection;

public interface ITaskEndpoint {

    Task save(Task task);

    void delete(Task task);

    void delete(String id);

    Collection<Task> findAll();

    Task findById(String id);

}
