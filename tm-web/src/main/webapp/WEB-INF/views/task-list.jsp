<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
  <tr>
    <th width="200" nowrap="nowrap">ID</th>
    <th width="200" nowrap="nowrap" align="left">NAME</th>
    <th width="100%" align="left">DESCRIPTION</th>
    <th width="150" nowrap="nowrap" align="center">STATUS</th>
    <th width="100" nowrap="nowrap" align="center">START</th>
    <th width="100" nowrap="nowrap" align="center">PROJECT</th>
    <th width="100" nowrap="nowrap" align="center">EDIT</th>
    <th width="100" nowrap="nowrap" align="center">DELETE</th>
  </tr>

	<c:forEach var="task" items="${tasks}" >
      <tr>
        <td>
            <c:out value="${task.id}" />
        </td>
        <td>
            <c:out value="${task.name}" />
        </td>
        <td>
            <c:out value="${task.description}" />
        </td>
        <td nowrap="nowrap" align="center">
            <c:out value="${task.status.displayName}" />
        </td>
		<td align="center">
            <fmt:formatDate pattern="dd.MM.yyyy" value="${task.created}" />
        </td>
        <td>
            <c:forEach var="project" items="${projects}">
                <c:if test="${project.id == task.projectId}">
                    <c:out value="${project.name}"/>
                </c:if>
            </c:forEach>
        </td>

		<td align="center">
            <a href="/task/edit/${task.id}">EDIT</a>
        </td>
		<td align="center">
            <a href="/task/delete/${task.id}">DELETE</a>
        </td>
      </tr>
	</c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px;">
	<button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>